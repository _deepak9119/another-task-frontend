import { Component, OnInit } from '@angular/core';
import {
  trigger,
  transition,
  query,
  style,
  animate,
  group
} from '@angular/animations';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('slider', [
      transition(":increment", group([
        query(':enter', [
          style({
            left: '100%'
          }),
          animate('0.5s ease-out', style('*'))
        ]),
        query(':leave', [
          animate('0.5s ease-out', style({
            left: '-100%'
          }))
        ])
      ])),
      transition(":decrement", group([
        query(':enter', [
          style({
            left: '-100%'
          }),
          animate('0.5s ease-out', style('*'))
        ]),
        query(':leave', [
          animate('0.5s ease-out', style({
            left: '100%'
          }))
        ])
      ])),
    ])
  ],
  styles: [`
         .slider-container {
            position: relative;
            height: 80vh;
            width: 100%;
            /* overflow: hidden; */
            border: none;
        } 
       .slide {
          position:absolute;
          width: 100%;
          right: 0;
          left: 0;
        }
        .slide img{
          width:100%
        }
  `]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  private _images: string[] = ['assets/img/e04ec5fb206073a5056c2bf2637e2cd7.jpg',
    'assets/img/7ba268ee73650e4b984c5541128bb4c2.jpg',
    'assets/img/54a17279bb49cca2a3c91c5aec4a80b7.jpg'
  ];
  selectedIndex: number = 0;

  get images() {
    return [this._images[this.selectedIndex]];
  }

  previous() {
    this.selectedIndex = Math.max(this.selectedIndex - 1, 0);
  }

  next() {
    this.selectedIndex = Math.min(this.selectedIndex + 1, this._images.length - 1);
  }

}
